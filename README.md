# Simple Todo

A simple todo API built with TypeScript.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
```
nodejs npm git
```

### Installing

Clone the project to your desired directory.

```
git clone https://alliedgorn@bitbucket.org/alliedgorn/todo.git
```

After you've cloned the project, next, download all dependencies by this command.

```
npm install
```

Wait until all dependencies are downloaded then build the project

```
npm run build
```

When project has built, simply run npm start to start the API

```
npm start
```

Now you are good to go. The API will be served at [http://localhost:3000](http://localhost:3000).

Although, you can check the API documents here [http://localhost:3000/api-docs](http://localhost:3000/api-docs).

## Running the tests

```
npm run test
```

## Built With

* [TypeScript](https://www.typescriptlang.org/) - The javascript superset.
* [Express](https://expressjs.com/) - Node.js web application framework

## Authors

* **Wutthikorn Kongprasopkij** - *Initial work*