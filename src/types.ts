const TYPES = {
  TodoItemRouter: Symbol.for("TodoItemRouter"),
  TodoItemRepository: Symbol.for("TodoItemRepository"),
  TodoItemService: Symbol.for("TodoItemService"),
  ErrorResponseHandler: Symbol.for("ErrorResponseHandler"),
  ValidationPolicy: Symbol.for("ValidationPolicy")
};

export { TYPES };