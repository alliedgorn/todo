import { Container } from "inversify";
import { TYPES } from "./types";
import TodoItemRepository from "./repositories/TodoItemRepository";
import LowdbTodoItemRepository from "./repositories/LowdbTodoItemRepository";
import TodoItemService from "./domain/services/TodoItemService";
import TodoItemRouter from "./routes/TodoItemRouter";
import ErrorResponseHandler from "./routes/ErrorResponseHandler";
import ValidationPolicy from "./routes/ValidationPolicy";

const container = new Container();
container.bind<TodoItemRouter>(TYPES.TodoItemRouter).to(TodoItemRouter);
container.bind<TodoItemRepository>(TYPES.TodoItemRepository).to(LowdbTodoItemRepository);
container.bind<TodoItemService>(TYPES.TodoItemService).to(TodoItemService);
container.bind<ErrorResponseHandler>(TYPES.ErrorResponseHandler).to(ErrorResponseHandler);
container.bind<ValidationPolicy>(TYPES.ValidationPolicy).to(ValidationPolicy);
export { container };