import * as shortid from "shortid";
import { injectable } from "../../node_modules/inversify";

@injectable()
export default class BaseRepository {
  protected generateId(): string {
    return shortid.generate();
  }
}