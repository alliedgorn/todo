import "reflect-metadata";
import * as lowdb from "lowdb";
import * as FileSync from "lowdb/adapters/FileSync";
import { injectable } from "inversify";

import TodoItem from "../domain/models/TodoItem";
import TodoItemRepository from "./TodoItemRepository";
import BaseRepository from "./BaseRepository";

@injectable()
export default class LowdbTodoItemRepository extends BaseRepository implements TodoItemRepository {
  private _adapter = new FileSync("storage/todo.json");
  private _db = lowdb(this._adapter);

  constructor() {
    super();
    this._db
      .defaults({ items: [] })
      .write();
  }

  public getAll(): TodoItem[] {
    return this._db
      .get('items')
      .value();
  }

  public getById(id: string): TodoItem {
    return <TodoItem>this._db
      .get('items')
      .find({ id })
      .value();
  }

  public create(item: TodoItem): string {
    item.id = this.generateId();
    const res =  this._db.get('items')
      .push(item)
      .write();
    return item.id;
  }

  public update(item: TodoItem) {
    this._db.get('items')
      .find({ id: item.id })
      .assign(item)
      .write();
    return item.id;
  }

  public delete(id: string) {
    this._db.get('items')
      .remove({ id })
      .write();
  }
}
