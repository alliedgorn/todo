import TodoItem from "../domain/models/TodoItem";

export default interface TodoItemRepository {
  getAll(): TodoItem[];
  getById(id: string): TodoItem;
  create(item: TodoItem): string;
  update(item: TodoItem): string;
  delete(id: string);
}