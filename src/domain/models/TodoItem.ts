import BaseModel from "./BaseModel";
import TodoStatus from "./TodoStatus";

export default class TodoItem extends BaseModel{
  constructor(public id: string, public subject: string, public content: string, public status: TodoStatus) {
    super();
  }
}