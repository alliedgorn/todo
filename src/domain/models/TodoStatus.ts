enum TodoStatus {
  Pending = "pending",
  Done = "done"
}

export default TodoStatus;