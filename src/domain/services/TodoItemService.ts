
import "reflect-metadata";
import { inject, injectable } from 'inversify';
import { TYPES } from "../../types";
import TodoItemVO from "../../views/TodoItemVO";
import TodoItemRepository from '../../repositories/TodoItemRepository';
import TodoItem from "../models/TodoItem";
import TodoItemCreateVO from "../../views/TodoItemCreateVO";
import BaseService from "./BaseService";
import ResourceNotFoundError from "../../errors/ResourceNotFoundError";

@injectable()
export default class TodoItemService extends BaseService {

  private _todoItemRepository: TodoItemRepository;

  constructor(@inject(TYPES.TodoItemRepository) todoItemRepository: TodoItemRepository) {
    super();
    this._todoItemRepository = todoItemRepository;
  }

  public getAll(): TodoItemVO[] {
    const todoItemList: TodoItem[] = this._todoItemRepository.getAll();
    return todoItemList.map(TodoItemVO.parse);
  }

  public getById(id: string): TodoItemVO {
    const todoItem: TodoItem = this._todoItemRepository.getById(id);
    if (!todoItem) {
      throw new ResourceNotFoundError(`Could not get todo item id: ${id}. Todo item not found.`);
    }
    return TodoItemVO.parse(todoItem);
  }

  public create(createItem: TodoItemCreateVO): TodoItemVO {
    const todoItem: TodoItem = createItem.buildTodo();
    const id = this._todoItemRepository.create(todoItem);
    const newTodoItem = this._todoItemRepository.getById(id);
    return TodoItemVO.parse(newTodoItem);
  }

  public update(updateItem: TodoItemVO): TodoItemVO {
    const todoItem: TodoItem = updateItem.buildTodo();
    const oldItem = this._todoItemRepository.getById(todoItem.id);
    if (!oldItem) {
      throw new ResourceNotFoundError(`Could not update item id: ${todoItem.id}. Todo item not found.`);
    }
    const id = this._todoItemRepository.update(todoItem);
    const updatedTodoItem = this._todoItemRepository.getById(id);
    return TodoItemVO.parse(updatedTodoItem);
  }

  public delete(id: string) {
    const todoItem: TodoItem = this._todoItemRepository.getById(id);
    if (!todoItem) {
      throw new ResourceNotFoundError(`Could not delete todo item id: ${id}. Todo item not found.`);
    }
    this._todoItemRepository.delete(id);
  }
}