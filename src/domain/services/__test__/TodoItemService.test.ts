import "jest";
import * as chai from "chai";
import TodoItemService from "../TodoItemService";
import TodoItemVO from "../../../views/TodoItemVO";
import TodoItemRepository from "../../../repositories/TodoItemRepository";
import TodoItem from "../../models/TodoItem";
import TodoItemCreateVO from "../../../views/TodoItemCreateVO";
import TodoStatus from "../../models/TodoStatus"
import ResourceNotFoundError from "../../../errors/ResourceNotFoundError";

const expect = chai.expect;

describe("getAll", () => {
  // Setup
  let sut: TodoItemService;
  let mockRepo: TodoItem[] = new Array(
    new TodoItem("id", "Wash clothes", "Wash clothes", TodoStatus.Pending),
    new TodoItem("id2", "Wash clothes", "Wash clothes", TodoStatus.Pending)
  );
  it("should return Array of TodoItemVO", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      getAll: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);

    const todoItems: TodoItemVO[] = sut.getAll();
    expect(todoItems).to.has.lengthOf(2);
    expect(todoItems).to.be.an("Array");
    expect(todoItems[0]).to.be.an.instanceof(TodoItemVO);
  });

  it("should include wash clothes task", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      getAll: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);

    const todoItems: TodoItemVO[] = sut.getAll();
    let washClothesTaskItem: TodoItemVO = todoItems.find(
      item => item.subject === "Wash clothes"
    );
    expect(washClothesTaskItem).to.exist;
    expect(washClothesTaskItem).to.have.all.keys([
      "id",
      "subject",
      "content",
      "status"
    ]);
  });
});

describe("createItem", () => {
  // Setup
  let sut: TodoItemService;
  let mockInput: TodoItemCreateVO = new TodoItemCreateVO("Watch a movie", "Watch Antman 2 at Major Ciniplex.");
  let mockRepo: TodoItem = new TodoItem("abc", "Watch a movie", "Watch Antman 2 at Major Ciniplex.", TodoStatus.Pending);

  it("should return TodoItemVO", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      create: jest.fn().mockReturnValue(mockRepo.id),
      getById: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);

    const todoItem: TodoItemVO = sut.create(mockInput);
    expect(todoItem).to.be.an.instanceof(TodoItemVO);
  });

  it("should return with relevant values from input", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      create: jest.fn().mockReturnValue(mockRepo.id),
      getById: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);

    const todoItem: TodoItemVO = sut.create(mockInput);
    expect(todoItem.subject).equals(mockInput.subject);
    expect(todoItem.content).equals(todoItem.content);
    expect(todoItem.id).is.not.empty;
    expect(todoItem.status).is.equals("pending");
  });

});

describe("getById", () => {
  // Setup
  let sut: TodoItemService;
  let mockId: string = "id";
  let mockRepo: TodoItem = new TodoItem(mockId, "Wash clothes", "Wash clothes", TodoStatus.Pending);
  it("should return TodoItemVO with correct id", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      getById: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);

    const todoItems: TodoItemVO = sut.getById(mockId);
    expect(todoItems).to.be.an.instanceof(TodoItemVO);
    expect(todoItems.id).equals(mockId);
  });
  it("should throw ResourceNotFoundError", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      getById: jest.fn().mockReturnValue(null)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);
    expect(sut.getById.bind(sut, mockId)).to.throw(ResourceNotFoundError);
  });
});

describe("update", () => {
  // Setup
  let sut: TodoItemService;
  let mockInput: TodoItemVO = new TodoItemVO("abc", "Wash clothes", "Wash clothes", TodoStatus.Pending);
  let mockRepo: TodoItem = new TodoItem("abc", "Wash clothes", "Wash clothes", TodoStatus.Pending);
  it("should return TodoItemVO with correct values", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      update: jest.fn().mockReturnValue(mockRepo.id),
      getById: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);
    const todoItems: TodoItemVO = sut.update(mockInput);
    expect(todoItems).to.be.an.instanceof(TodoItemVO);
    expect(todoItems.id).equals(mockInput.id);
    expect(todoItems.subject).equals(mockInput.subject);
    expect(todoItems.content).equals(mockInput.content);
    expect(todoItems.status).equals(mockInput.status);
  });
  it("should throw ResourceNotFoundError", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      update: jest.fn().mockResolvedValue(mockRepo.id),
      getById: jest.fn().mockReturnValue(null)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);
    expect(sut.update.bind(sut, mockInput)).to.throw(ResourceNotFoundError);
  });
});

describe("delete", () => {
  // Setup
  let sut: TodoItemService;
  let mockInput: TodoItemVO = new TodoItemVO("abc", "Wash clothes", "Wash clothes", TodoStatus.Pending);
  let mockRepo: TodoItem = new TodoItem("abc", "Wash clothes", "Wash clothes", TodoStatus.Pending);
  it("should not throw error", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      delete: jest.fn().mockResolvedValue(mockRepo),
      getById: jest.fn().mockReturnValue(mockRepo)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);
    sut.delete(mockInput.id);
  });
  it("should throw ResourceNotFoundError", () => {
    const Mock = jest.fn<TodoItemRepository>(() => ({
      delete: jest.fn().mockResolvedValue(mockRepo),
      getById: jest.fn().mockReturnValue(null)
    }));
    const mock = new Mock();
    sut = new TodoItemService(mock);
    expect(sut.delete.bind(sut, mockInput.id)).to.throw(ResourceNotFoundError);
  });
});