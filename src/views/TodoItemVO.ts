import TodoItem from '../domain/models/TodoItem';
import TodoStatus from '../domain/models/TodoStatus';

export default class TodoItemVO {
  constructor(public id: string, public subject: string, public content: string, public status: TodoStatus) {
  }
  
  public buildTodo(): TodoItem {
    return new TodoItem(this.id, this.subject, this.content, this.status);
  }

  static parse(todoItem: TodoItem): TodoItemVO {
    return new TodoItemVO(todoItem.id, todoItem.subject, todoItem.content, todoItem.status);
  }

}