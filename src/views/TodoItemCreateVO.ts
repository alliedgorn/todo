import TodoItem from "../domain/models/TodoItem";
import TodoStatus from "../domain/models/TodoStatus";

export default class TodoItemCreateVO {
  
  constructor(public subject: string, public content: string) {
  }

  public buildTodo(): TodoItem {
    return new TodoItem(null, this.subject, this.content, TodoStatus.Pending);
  }
}