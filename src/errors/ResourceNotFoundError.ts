export default class ResourceNotFoundError extends Error {
  constructor(public error: string) {
      super(error);
      Object.setPrototypeOf(this, ResourceNotFoundError.prototype);
  }
}