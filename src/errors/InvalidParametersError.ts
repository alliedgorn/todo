export default class InvalidParametersError extends Error {
  constructor(public errors: any[]) {
      super(errors.join(', '));
      Object.setPrototypeOf(this, InvalidParametersError.prototype);
  }
}