import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as swaggerUi from 'swagger-ui-express';
import * as expressValidator from 'express-validator';
import { container } from './inversify.config';
import TodoItemRouter from './routes/TodoItemRouter';
import { TYPES } from './types';
import swaggerDocument from './swagger.json';

// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: express.Application;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
    this.swagger();
  }

  // Configure Express middleware.
  private middleware(): void {
    this.express.use(logger('dev'));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(expressValidator());
  }

  // Configure API endpoints.
  private routes(): void {

    const todoItemRouter = container.get<TodoItemRouter>(TYPES.TodoItemRouter);
    let router = express.Router();
    this.express.use('/', router);
    this.express.use('/api/todo/items', todoItemRouter.router);
  }

  private swagger(): void {
    this.express.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

}

export default new App().express;