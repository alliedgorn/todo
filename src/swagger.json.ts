import TodoStatus from "./domain/models/TodoStatus";

export default {
  swagger: "2.0",
  info: {
    version: "1.0.0",
    title: "Todo API",
    description: "A simple todo API that store task list."
  },
  schemes: ["http"],
  host: "localhost:3000",
  basePath: "/api/todo",
  paths: {
    "/items": {
      get: {
        summary: "Gets todo items",
        description: "Returns a list containing all todo items.",
        produces: ["application/json"],
        responses: {
          "200": {
            description: "A list of todo items",
            schema: {
              type: "array",
              items: {
                properties: {
                  id: {
                    type: "string"
                  },
                  subject: {
                    type: "string"
                  },
                  content: {
                    type: "string"
                  },
                  status: {
                    type: "string"
                  }
                }
              }
            }
          }
        }
      },
      post: {
        summary: "Create new todo item",
        consumes: ["application/json"],
        produces: ["application/json"],
        parameters: [
          {
            in: "body",
            name: "body",
            description: "parameters to create a todo items",
            required: true,
            schema: {
              properties: {
                subject: {
                  type: "string"
                },
                content: {
                  type: "string"
                }
              }
            }
          }
        ],
        responses: {
          "200": {
            description: "new todo item",
            schema: {
              properties: {
                id: {
                  type: "string"
                },
                subject: {
                  type: "string"
                },
                content: {
                  type: "string"
                },
                status: {
                  type: "string",
                  enum: [TodoStatus.Done, TodoStatus.Pending]
                }
              }
            }
          },
          "400": {
            description: "Invalid input",
            schema: {
              properties: {
                errors: {
                  type: "array",
                  items: {
                    properties: {
                      location: {
                        type: "string"
                      },
                      param: {
                        type: "string"
                      },
                      msg: {
                        type: "string"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      put: {
        summary: "Update existing todo item",
        consumes: ["application/json"],
        produces: ["application/json"],
        parameters: [
          {
            in: "body",
            name: "body",
            description: "an updated todo item",
            required: true,
            schema: {
              properties: {
                id: {
                  type: "string"
                },
                subject: {
                  type: "string"
                },
                content: {
                  type: "string"
                },
                status: {
                  type: "string",
                  enum: [TodoStatus.Done, TodoStatus.Pending]
                }
              }
            }
          }
        ],
        responses: {
          "200": {
            description: "Update completed.",
            schema: {
              properties: {
                id: {
                  type: "string"
                },
                subject: {
                  type: "string"
                },
                content: {
                  type: "string"
                },
                status: {
                  type: "string",
                  enum: [TodoStatus.Done, TodoStatus.Pending]
                }
              }
            }
          },
          "400": {
            description: "Invalid input",
            schema: {
              properties: {
                errors: {
                  type: "array",
                  items: {
                    properties: {
                      location: {
                        type: "string"
                      },
                      param: {
                        type: "string"
                      },
                      msg: {
                        type: "string"
                      }
                    }
                  }
                }
              }
            }
          },
          "404": {
            description: "Resource not found",
            schema: {
              properties: {
                error: {
                  type: "string"
                }
              }
            }
          }
        }
      }
    },
    "/items/{id}": {
      get: {
        summary: "Get a todo item by id",
        produces: ["application/json"],
        parameters: [
          {
            in: "path",
            name: "id",
            description: "an existing todo item id",
            required: true,
            schema: {
              type: "string"
            }
          }
        ],
        responses: {
          "200": {
            description: "A todo item.",
            schema: {
              properties: {
                id: {
                  type: "string"
                },
                subject: {
                  type: "string"
                },
                content: {
                  type: "string"
                },
                status: {
                  type: "string",
                  enum: [TodoStatus.Done, TodoStatus.Pending]
                }
              }
            }
          },
          "404": {
            description: "Resource not found",
            schema: {
              properties: {
                error: {
                  type: "string"
                }
              }
            }
          }
        }
      },
      delete: {
        summary: "Delete existing todo item",
        produces: ["application/json"],
        parameters: [
          {
            in: "path",
            name: "id",
            description: "todo item id",
            required: true,
            schema: {
              type: "string"
            }
          }
        ],
        responses: {
          "200": {
            description: "Todo item deleted."
          },
          "404": {
            description: "Resource not found",
            schema: {
              properties: {
                error: {
                  type: "string"
                }
              }
            }
          }
        }
      }
    }
  }
};
