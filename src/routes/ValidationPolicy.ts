import { Result } from 'express-validator/check';
import { validationResult } from 'express-validator/check';
import { injectable } from 'inversify';
import InvalidParametersError from '../errors/InvalidParametersError';

@injectable()
export default class ValidationPolicy {
  public check(req) {
    const errors: Result = validationResult(req);
    if (!errors.isEmpty()) {
      throw new InvalidParametersError(errors.array());
    }
  }
}