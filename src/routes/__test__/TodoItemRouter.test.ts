import "jest";
import * as chai from "chai";
import chaiHttp = require("chai-http");

import app from "../../../src/App";

chai.use(chaiHttp);
const expect = chai.expect;

describe("GET api/todo/items", () => {
  it("responds with JSON array", () => {
    return chai
      .request(app)
      .get("/api/todo/items")
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("array");
      });
  });
});

describe("POST api/todo/items", () => {
  it("responds with JSON object and relevant values", () => {
    const mockItem = {
      subject: "POST Watch the movie",
      content: "POST Watch Antman2 tonight"
    };
    return chai
      .request(app)
      .post("/api/todo/items")
      .send(mockItem)
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("object");
        expect(res.body).to.have.all.keys([
          "id",
          "subject",
          "content",
          "status"
        ]);
        expect(res.body.id).is.not.empty;
        expect(res.body.subject).is.equal(mockItem.subject);
        expect(res.body.content).is.equal(mockItem.content);
        expect(res.body.status).is.equal("pending");
      });
  });
  it("responds with status 400 when subject is missing from parameters", () => {
    const mockItem = {
      content: "POST Watch Antman2 tonight"
    };
    return chai
      .request(app)
      .post("/api/todo/items")
      .send(mockItem)
      .then(res => {
        expect(res.status).to.equal(400);
      });
  });
});

describe("GET api/todo/items/:id", () => {
  const mockItem = {
    subject: "GET id Watch the movie",
    content: "GET id Watch Antman2 tonight"
  };
  it("responds with JSON object with correct keys", async () => {
    const res = await chai
      .request(app)
      .post("/api/todo/items")
      .send(mockItem);

    return chai
      .request(app)
      .get("/api/todo/items/" + res.body.id)
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("object");
        expect(res.body).to.have.all.keys([
          "id",
          "subject",
          "content",
          "status"
        ]);
      });
  });
  it("responds with status 404", async () => {
    return chai
      .request(app)
      .get("/api/todo/items/inappropriatedId")
      .then(res => {
        expect(res.status).to.equal(404);
        expect(res.body.error).exist;
      });
  });
});

describe("PUT api/todo/items", () => {
  it("responds with JSON object and relevant values", async () => {
    const mockItem = {
      subject: "PUT Watch the movie",
      content: "PUT Watch Antman2 tonight"
    };
    const editedSubject = "Have a dinner";
    const editedContent = "Have a dinner at Oishi Grand";

    const res = await chai
      .request(app)
      .post("/api/todo/items")
      .send(mockItem);

    const item = res.body;
    item.subject = editedSubject;
    item.content = editedContent;

    return chai
      .request(app)
      .put("/api/todo/items")
      .send(item)
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("object");
        expect(res.body).to.have.all.keys([
          "id",
          "subject",
          "content",
          "status"
        ]);
        expect(res.body.subject).equals(editedSubject);
        expect(res.body.content).equals(editedContent);
      });
  });
  it("responds with status 404", async () => {
    const mockItem = {
      id: "inappropriatedId",
      subject: "PUT Watch the movie",
      content: "PUT Watch Antman2 tonight",
      status: "pending"
    };
    return chai
      .request(app)
      .put("/api/todo/items")
      .send(mockItem)
      .then(res => {
        expect(res.status).to.equal(404);
        expect(res.body.error).exist;
      });
  });
  it("responds with status 400", async () => {
    const mockItem = {
      id: "id",
      subject: "",
      content: "",
      status: "incorrect status"
    };
    return chai
      .request(app)
      .put("/api/todo/items")
      .send(mockItem)
      .then(res => {
        expect(res.status).to.equal(400);
      });
  });
});

describe("DELETE api/todo/items/:id", () => {
  const mockItem = {
    subject: "DELETE Watch the movie",
    content: "DELETE Watch Antman2 tonight"
  };
  it("responds with status 200", async () => {
    const res = await chai
      .request(app)
      .post("/api/todo/items")
      .send(mockItem);

    const item = res.body;

    return chai
      .request(app)
      .del("/api/todo/items/" + item.id)
      .then(res => {
        expect(res.status).to.equal(200);
      });
  });
  it("responds with status 404", async () => {
    return chai
      .request(app)
      .del("/api/todo/items/" + "inappropriatedId")
      .then(res => {
        expect(res.status).to.equal(404);
        expect(res.body.error).exist;
      });
  });
});
