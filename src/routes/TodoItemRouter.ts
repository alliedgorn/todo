import { Router, Request, Response, NextFunction } from "express";
import { injectable, inject } from "../../node_modules/inversify";
import TodoItemService from "../domain/services/TodoItemService";
import { TYPES } from "../types";
import TodoItemCreateVO from "../views/TodoItemCreateVO";
import TodoItemVO from "../views/TodoItemVO";
import ErrorResponseHandler from "./ErrorResponseHandler";
import { check } from "express-validator/check";
import ValidationPolicy from "./ValidationPolicy";
import TodoStatus from "../domain/models/TodoStatus";

@injectable()
export default class TodoItemRouter {
  router: Router;
  private _todoItemService: TodoItemService;
  private _errorHandler: ErrorResponseHandler;
  private _validationPolicy: ValidationPolicy;
  constructor(
    @inject(TYPES.ValidationPolicy) private validationPolicy: ValidationPolicy,
    @inject(TYPES.ErrorResponseHandler) private errorHandler: ErrorResponseHandler,
    @inject(TYPES.TodoItemService) private todoItemService: TodoItemService
  ) {
    this._todoItemService = todoItemService;
    this._errorHandler = errorHandler;
    this._validationPolicy = validationPolicy;
    this.router = Router();

    this.getAll = this.getAll.bind(this);
    this.create = this.create.bind(this);
    this.get = this.get.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
    this.init();
  }

  public getAll(req: Request, res: Response, next: NextFunction) {
    this._validationPolicy.check(req);
    return res.status(200).send(this._todoItemService.getAll());
  }

  public create(req: Request, res: Response, next: NextFunction) {
    this._validationPolicy.check(req);
    const body = req.body;
    const createItem: TodoItemCreateVO = new TodoItemCreateVO(
      body.subject,
      body.content
    );
    const result = this._todoItemService.create(createItem);
    return res.status(200).send(result);
  }

  public get(req: Request, res: Response, next: NextFunction) {
    this._validationPolicy.check(req);
    const result: TodoItemVO = this._todoItemService.getById(req.params.id);
    return res.status(200).send(result);
  }

  public update(req: Request, res: Response, next: NextFunction) {
    this._validationPolicy.check(req);
    const body = req.body;
    const updateItem: TodoItemVO = new TodoItemVO(
      body.id,
      body.subject,
      body.content,
      body.status
    );
    const result = this._todoItemService.update(updateItem);
    return res.status(200).send(result);
  }

  public delete(req: Request, res: Response, next: NextFunction) {
    this._validationPolicy.check(req);
    this._todoItemService.delete(req.params.id);
    return res.sendStatus(200);
  }

  public init() {
    this.router.get("/", this.getAll, this._errorHandler.handle);
    this.router.get("/:id", this.get, this._errorHandler.handle);
    this.router.post("/", [
      check('subject', 'subject must be present.').isLength({ min: 1 })
    ], this.create, this._errorHandler.handle);
    this.router.put("/", [
      check('subject', 'subject must be present.').isLength({ min: 1 }),
      check('status', 'incorrect status.').isIn([TodoStatus.Pending, TodoStatus.Done])
    ], this.update, this._errorHandler.handle);
    this.router.delete("/:id", this.delete, this._errorHandler.handle);
  }
}
