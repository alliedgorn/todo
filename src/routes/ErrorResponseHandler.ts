import { injectable } from 'inversify';
import ResourceNotFoundError from '../errors/ResourceNotFoundError';
import InvalidParametersError from '../errors/InvalidParametersError';
import ErrorListVO from '../views/errors/ErrorListVO';
import ErrorVO from '../views/errors/ErrorVO';

@injectable()
export default class ErrorResponseHandler {
  public handle(err, req, res, next) {
    if (err instanceof ResourceNotFoundError) {
      return res.status(404).send(new ErrorVO(err.error));
    } else if (err instanceof InvalidParametersError) {
      return res.status(400).send(new ErrorListVO(err.errors));
    }
    return res.sendStatus(500);
  }
}